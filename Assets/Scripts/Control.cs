using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control : MonoBehaviour
{
    private Animator anim;
    CharacterController characterController;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    public float speed = 1.2f;
    private Vector3 moveDirection = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");

        transform.Translate(new Vector3(horizontal, 0, vertical) * (speed * Time.deltaTime));
        //anim.SetFloat("speed", Input.GetAxis("horizontal"));
        anim.SetFloat("speed", Input.GetAxis("Vertical"));
        if(Input.GetButtonDown("Jump"))
        {
            anim.SetTrigger("jump");
        }
        if (characterController.isGrounded)
        {

           
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            moveDirection *= speed;

            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;
            }
        }
        moveDirection.y -= gravity * Time.deltaTime;
        characterController.Move(moveDirection * Time.deltaTime);
    }
}
