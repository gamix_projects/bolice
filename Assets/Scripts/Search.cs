using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Search : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //Search with name
        GameObject go1 = GameObject.Find("SearchWithName");
        Debug.Log(go1 == null ? "Pas d'objet" : "Y a un objet");
        //Search with tag
        GameObject go2 = GameObject.FindGameObjectWithTag("search");
        GameObject[] arrayGo2 = GameObject.FindGameObjectsWithTag("search");
        // Search with component
        GameObject go3 = FindObjectOfType<Rigidbody>().gameObject;
        BoxCollider bx1 = FindObjectOfType<Rigidbody>().gameObject.GetComponent<BoxCollider>();
        Rigidbody[] arrayRb = FindObjectsOfType<Rigidbody>();
    }
}
